//! Error handling.

pub type RnpResult = u32;
pub const RNP_SUCCESS: RnpResult = 0x00000000;

// Common error codes
pub const RNP_ERROR_GENERIC: RnpResult = 0x10000000;
pub const RNP_ERROR_BAD_FORMAT: RnpResult = 0x10000001;
pub const RNP_ERROR_BAD_PARAMETERS: RnpResult = 0x10000002;
pub const RNP_ERROR_NOT_IMPLEMENTED: RnpResult = 0x10000003;
pub const RNP_ERROR_NOT_SUPPORTED: RnpResult = 0x10000004;
pub const RNP_ERROR_OUT_OF_MEMORY: RnpResult = 0x10000005;
pub const RNP_ERROR_SHORT_BUFFER: RnpResult = 0x10000006;
pub const RNP_ERROR_NULL_POINTER: RnpResult = 0x10000007;

// Storage
pub const RNP_ERROR_ACCESS: RnpResult = 0x11000000;
pub const RNP_ERROR_READ: RnpResult = 0x11000001;
pub const RNP_ERROR_WRITE: RnpResult = 0x11000002;

// Crypto
pub const RNP_ERROR_BAD_STATE: RnpResult = 0x12000000;
pub const RNP_ERROR_MAC_INVALID: RnpResult = 0x12000001;
pub const RNP_ERROR_SIGNATURE_INVALID: RnpResult = 0x12000002;
pub const RNP_ERROR_KEY_GENERATION: RnpResult = 0x12000003;
pub const RNP_ERROR_BAD_PASSWORD: RnpResult = 0x12000004;
pub const RNP_ERROR_KEY_NOT_FOUND: RnpResult = 0x12000005;
pub const RNP_ERROR_NO_SUITABLE_KEY: RnpResult = 0x12000006;
pub const RNP_ERROR_DECRYPT_FAILED: RnpResult = 0x12000007;
pub const RNP_ERROR_RNG: RnpResult = 0x12000008;
pub const RNP_ERROR_SIGNING_FAILED: RnpResult = 0x12000009;
pub const RNP_ERROR_NO_SIGNATURES_FOUND: RnpResult = 0x1200000a;

pub const RNP_ERROR_SIGNATURE_EXPIRED: RnpResult = 0x1200000b;

// Parsing
pub const RNP_ERROR_NOT_ENOUGH_DATA: RnpResult = 0x13000000;
pub const RNP_ERROR_UNKNOWN_TAG: RnpResult = 0x13000001;
pub const RNP_ERROR_PACKET_NOT_CONSUMED: RnpResult = 0x13000002;
pub const RNP_ERROR_NO_USERID: RnpResult = 0x13000003;
pub const RNP_ERROR_EOF: RnpResult = 0x13000004;

// Used by helper functions.
pub type Result<T> = std::result::Result<T, RnpResult>;

/// Converts the status code to a string.
pub fn rnp_result_to_str(result: RnpResult) -> &'static str {
    unsafe {
        std::ffi::CStr::from_ptr(super::rnp_result_to_string(result))
            .to_str().expect("utf8")
    }
}

#[macro_export]
macro_rules! rnp_try {
    ( $fn: ident ( $($arg: expr),* ) ) => {
        match $fn($($arg),*) {
            RNP_SUCCESS => (),
            r => Err(anyhow::anyhow!("{}: {}", stringify!($fn),
                                     rnp_result_to_str(r)))?,
        }
    };
}
