use std::{
    ffi::{CStr, CString},
    fmt,
    io::Read,
    path::Path,
    ptr::null_mut,
};

use libc::{
    c_char,
};

use anyhow::Context;
use chrono::{DateTime, offset::Utc};
use structopt::StructOpt;

use librnp_sys::*;

use super::{
    dates,
    Error,
    FINGERPRINT,
    Result,
    rnp_input_bytes_ref,
};

#[derive(Debug, StructOpt)]
#[structopt(about = "An implementation of the \
                     Stateless OpenPGP Command Line Interface \
                     using RNP")]
pub enum SOP {
    /// Prints version information.
    Version {
        #[structopt(long, conflicts_with("backend"))]
        extended: bool,
        #[structopt(long, conflicts_with("extended"))]
        backend: bool,
    },
    /// Generates a Secret Key.
    GenerateKey {
        /// Don't ASCII-armor output.
        #[structopt(long)]
        no_armor: bool,
        /// UserIDs for the generated key.
        userids: Vec<String>,
    },
    /// Extracts a Certificate from a Secret Key.
    ExtractCert {
        /// Don't ASCII-armor output.
        #[structopt(long)]
        no_armor: bool,
    },
    /// Creates Detached Signatures.
    Sign {
        /// Don't ASCII-armor output.
        #[structopt(long)]
        no_armor: bool,
        /// Sign binary data or UTF-8 text.
        #[structopt(default_value = "binary", long = "as")]
        as_: SignAs,
        /// Keys for signing.
        keys: Vec<String>,
    },
    /// Verifies Detached Signatures.
    Verify {
        /// Consider signatures before this date invalid.
        #[structopt(long, parse(try_from_str = dates::parse_bound_round_down))]
        not_before: Option<DateTime<Utc>>,
        /// Consider signatures after this date invalid.
        #[structopt(long, parse(try_from_str = dates::parse_bound_round_up))]
        not_after: Option<DateTime<Utc>>,
        /// Signatures to verify.
        signatures: String,
        /// Certs for verification.
        certs: Vec<String>,
    },
    /// Encrypts a Message.
    Encrypt {
        /// Don't ASCII-armor output.
        #[structopt(long)]
        no_armor: bool,
        /// Encrypt binary data, UTF-8 text, or MIME data.
        #[structopt(default_value = "binary", long = "as")]
        as_: EncryptAs,
        /// Encrypt with passwords.
        #[structopt(long, number_of_values = 1)]
        with_password: Vec<String>,
        /// Keys for signing.
        #[structopt(long, number_of_values = 1)]
        sign_with: Vec<String>,
        /// Encrypt for these certs.
        certs: Vec<String>,
    },
    /// Decrypts a Message.
    Decrypt {
        /// Write the session key here.
        #[structopt(long)]
        session_key_out: Option<String>,
        /// Try to decrypt with this session key.
        #[structopt(long, number_of_values = 1)]
        with_session_key: Vec<String>,
        /// Try to decrypt with this password.
        #[structopt(long, number_of_values = 1)]
        with_password: Vec<String>,
        /// Write verification result here.
        #[structopt(long)]
        verify_out: Option<String>,
        /// Certs for verification.
        #[structopt(long, number_of_values = 1)]
        verify_with: Vec<String>,
        /// Consider signatures before this date invalid.
        #[structopt(long, parse(try_from_str = dates::parse_bound_round_down))]
        verify_not_before: Option<DateTime<Utc>>,
        /// Consider signatures after this date invalid.
        #[structopt(long, parse(try_from_str = dates::parse_bound_round_up))]
        verify_not_after: Option<DateTime<Utc>>,
        /// Try to decrypt with this key.
        key: Vec<String>,
    },
    /// Converts binary OpenPGP data to ASCII
    Armor {
        /// Indicates the kind of data
        #[structopt(long, default_value = "auto")]
        label: ArmorKind,
    },
    /// Converts ASCII OpenPGP data to binary
    Dearmor {
    },
    /// Unsupported subcommand.
    #[structopt(external_subcommand)]
    Unsupported(Vec<String>),
}

#[derive(Clone, Copy, Debug)]
pub enum SignAs {
    Binary,
    Text,
}

impl std::str::FromStr for SignAs {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        match s {
            "binary" => Ok(SignAs::Binary),
            "text" => Ok(SignAs::Text),
            _ => Err(anyhow::anyhow!(
                "{:?}, expected one of {{binary|text}}", s)),
        }
    }
}

impl fmt::Display for SignAs {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SignAs::Binary => f.write_str("binary"),
            SignAs::Text => f.write_str("text"),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum EncryptAs {
    Binary,
    Text,
    MIME,
}

impl std::str::FromStr for EncryptAs {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        match s {
            "binary" => Ok(EncryptAs::Binary),
            "text" => Ok(EncryptAs::Text),
            "mime" => Ok(EncryptAs::MIME),
            _ => Err(anyhow::anyhow!(
                "{}, expected one of {{binary|text|mime}}", s)),
        }
    }
}

impl fmt::Display for EncryptAs {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EncryptAs::Binary => f.write_str("binary"),
            EncryptAs::Text => f.write_str("text"),
            EncryptAs::MIME => f.write_str("mime"),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum ArmorKind {
    Auto,
    Sig,
    Key,
    Cert,
    Message,
}

impl std::str::FromStr for ArmorKind {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        match s {
            "auto" => Ok(ArmorKind::Auto),
            "sig" => Ok(ArmorKind::Sig),
            "key" => Ok(ArmorKind::Key),
            "cert" => Ok(ArmorKind::Cert),
            "message" => Ok(ArmorKind::Message),
            _ => Err(anyhow::anyhow!(
                "{:?}, expected one of \
                 {{auto|sig|key|cert|message}}", s)),
        }
    }
}

impl fmt::Display for ArmorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ArmorKind::Auto => f.write_str("auto"),
            ArmorKind::Sig => f.write_str("sig"),
            ArmorKind::Key => f.write_str("key"),
            ArmorKind::Cert => f.write_str("cert"),
            ArmorKind::Message => f.write_str("message"),
        }
    }
}


fn is_special_designator<S: AsRef<str>>(file: S) -> bool {
    file.as_ref().starts_with("@")
}

/// Loads the given (special) file.
pub fn load_file<S: AsRef<str>>(file: S) -> Result<std::fs::File> {
    let f = file.as_ref();

    if is_special_designator(f) {
        if Path::new(f).exists() {
            return Err(anyhow::Error::from(Error::AmbiguousInput))
                .context(format!("File {:?} exists", f));
        }

        #[cfg(unix)]
        {
            if f.starts_with("@FD:")
                && f[4..].chars().all(|c| c.is_ascii_digit())
            {
                use std::os::unix::io::{RawFd, FromRawFd};

                let fd: RawFd = f[4..].parse()
                    .map_err(|_| Error::UnsupportedSpecialPrefix)?;
                let f = unsafe {
                    std::fs::File::from_raw_fd(fd)
                };
                return Ok(f);
            }
        }

        return Err(anyhow::Error::from(Error::UnsupportedSpecialPrefix));
    }

    std::fs::File::open(f).map_err(|_| Error::MissingInput)
            .context(format!("Failed to open file {:?}", f))
}

/// Creates the given (special) file.
pub fn create_file<S: AsRef<str>>(file: S) -> Result<std::fs::File> {
    let f = file.as_ref();

    if is_special_designator(f) {
        if Path::new(f).exists() {
            return Err(anyhow::Error::from(Error::AmbiguousInput))
                .context(format!("File {:?} exists", f));
        }

        return Err(anyhow::Error::from(Error::UnsupportedSpecialPrefix));
    }

    if Path::new(f).exists() {
        return Err(anyhow::Error::from(Error::OutputExists))
            .context(format!("File {:?} exists", f));
    }

    std::fs::File::create(f).map_err(|_| Error::MissingInput) // XXX
            .context(format!("Failed to create file {:?}", f))
}

#[derive(serde::Deserialize, Debug)]
struct ImportResults {
    keys: Vec<ImportResult>,
}

#[derive(serde::Deserialize, Debug)]
struct ImportResult {
    public: String,
    secret: String,
    fingerprint: String,
}

/// Loads the certs given by the (special) files.
pub unsafe fn load_certs(ffi: rnp_ffi_t, files: &[String])
                         -> Result<Vec<rnp_key_handle_t>> {
    let mut handles = Vec::new();
    for f in files {
        let mut r = load_file(&f)?;
        let mut buf = Vec::new();
        r.read_to_end(&mut buf)?;
        let input = rnp_input_bytes_ref(&buf)?;

        let mut results_raw: *mut c_char = null_mut();
        rnp_try!(rnp_import_keys(ffi, input, RNP_LOAD_SAVE_PUBLIC_KEYS,
                                 &mut results_raw as *mut _));
        rnp_try!(rnp_input_destroy(input));

        if results_raw.is_null() {
            continue;
        }

        let results: ImportResults =
            serde_json::from_slice(CStr::from_ptr(results_raw).to_bytes())?;
        for result in results.keys {
            let fp = CString::new(result.fingerprint).expect("nul safe");
            let mut handle: rnp_key_handle_t = null_mut();
            rnp_try!(rnp_locate_key(ffi,
                                    FINGERPRINT,
                                    fp.as_ptr(),
                                    &mut handle as *mut _));
            assert!(! handle.is_null());

            let mut is_primary = false;
            rnp_try!(rnp_key_is_primary(handle, &mut is_primary));
            if is_primary {
                handles.push(handle);
            } else {
                rnp_try!(rnp_key_handle_destroy(handle));
            }
        }

        rnp_buffer_destroy(results_raw as *mut _);
    }
    Ok(handles)
}

/// Loads the KEY given by the (special) files.
pub unsafe fn load_keys(ffi: rnp_ffi_t, files: &[String])
                        -> Result<Vec<rnp_key_handle_t>> {
    let mut handles = Vec::new();
    for f in files {
        let mut r = load_file(&f)?;
        let mut buf = Vec::new();
        r.read_to_end(&mut buf)?;
        let input = rnp_input_bytes_ref(&buf)?;

        let mut results_raw: *mut c_char = null_mut();
        rnp_try!(rnp_import_keys(ffi, input, RNP_LOAD_SAVE_SECRET_KEYS,
                                 &mut results_raw as *mut _));
        rnp_try!(rnp_input_destroy(input));

        if results_raw.is_null() {
            continue;
        }

        let results: ImportResults =
            serde_json::from_slice(CStr::from_ptr(results_raw).to_bytes())?;
        for result in results.keys {
            let fp = CString::new(result.fingerprint).expect("nul safe");
            let mut handle: rnp_key_handle_t = null_mut();
            rnp_try!(rnp_locate_key(ffi,
                                    FINGERPRINT,
                                    fp.as_ptr(),
                                    &mut handle as *mut _));
            assert!(! handle.is_null());

            let mut is_primary = false;
            rnp_try!(rnp_key_is_primary(handle, &mut is_primary));
            if is_primary {
                handles.push(handle);
            } else {
                rnp_try!(rnp_key_handle_destroy(handle));
            }
        }

        rnp_buffer_destroy(results_raw as *mut _);
    }
    Ok(handles)
}

/// Pull strings from files and convert them to passwords.
pub fn frob_passwords(p: Vec<String>) -> Result<Vec<String>> {
    // XXX: Maybe do additional checks.
    let mut passwords: Vec<String> = Vec::new();
    for pfile in p {
        let mut r = load_file(&pfile)?;
        let mut buf = Vec::new();
        r.read_to_end(&mut buf)?;
        passwords.push(String::from_utf8(buf)?.trim_end().to_string());
    }
    Ok(passwords)
}
