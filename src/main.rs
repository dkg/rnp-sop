//! An implementation of the Stateless OpenPGP Command Line Interface
//! using RNP.
//!
//! This implements a subset of the [Stateless OpenPGP Command Line
//! Interface] using the RNP OpenPGP implementation.
//!
//!   [Stateless OpenPGP Command Line Interface]: https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/

use std::{
    ptr::{null, null_mut},
    ffi::{
        CStr,
        CString,
    },
    io::{stdin, stdout, Read, Write},
    time::{UNIX_EPOCH, Duration},
};

use libc::{
    c_char,
    c_void,
    size_t,
};

use anyhow::Context as _;
use chrono::{DateTime, offset::Utc};
use structopt::StructOpt;

use librnp_sys::*;

mod errors;
use errors::{Error, print_error_chain};
type Result<T> = anyhow::Result<T>;

mod cli;
use cli::{
    SOP, SignAs, EncryptAs,
    load_file, create_file, load_certs, load_keys, frob_passwords,
};
mod dates;

fn main() {
    use std::process::exit;

    match unsafe { real_main() } {
        Ok(()) => (),
        Err(e) => {
            print_error_chain(&e);
            if let Ok(e) = e.downcast::<Error>() {
                exit(e.into())
            }
            exit(1);
        },
    }
}

const GPG: *const c_char = b"GPG\0".as_ptr() as *const _;
const FINGERPRINT: *const c_char = b"fingerprint\0".as_ptr() as *const _;

unsafe fn real_main() -> Result<()> {
    let mut ffi = null_mut();
    rnp_try!(rnp_ffi_create(&mut ffi as *mut _, GPG, GPG));

    match SOP::from_args() {
        SOP::Version { extended, backend } => {
            let backend_ver = CStr::from_ptr(rnp_version_string_full()).to_str().unwrap();
            let cli_ver = env!("CARGO_PKG_VERSION");
            if backend {
                println!("RNP {}", backend_ver);
            } else if extended {
                println!("RNP-sop {}", cli_ver);
                println!("RNP {}", backend_ver);
            } else {
                println!("RNP-sop {}", cli_ver);
            }
        },

        SOP::GenerateKey { no_armor, userids, } => {
            let mut op = null_mut();

            // Generate the primary key.
            rnp_try!(rnp_op_generate_create(&mut op as *mut _,
                                            ffi,
                                            b"rsa\0".as_ptr() as *const _));

            for u in &userids {
                rnp_try!(rnp_op_generate_set_userid(
                    op as *mut _,
                    CString::new(u.as_bytes())?.as_ptr()));
            }

            rnp_try!(rnp_op_generate_execute(op));
            let mut primary = null_mut();
            rnp_try!(rnp_op_generate_get_key(op,
                                             &mut primary as *mut _));
            rnp_try!(rnp_op_generate_destroy(op));

            // Generate the subkey.
            rnp_try!(rnp_op_generate_subkey_create(&mut op as *mut _,
                                                   ffi,
                                                   primary,
                                                   b"rsa\0".as_ptr() as *const _));

            rnp_try!(rnp_op_generate_execute(op));
            rnp_try!(rnp_op_generate_destroy(op));

            let sink = rnp_stdout()?;
            rnp_try!(rnp_key_export(primary, sink,
                                    RNP_KEY_EXPORT_SECRET
                                    | RNP_KEY_EXPORT_SUBKEYS
                                    | if no_armor {
                                        0
                                    } else {
                                        RNP_KEY_EXPORT_ARMORED
                                    }));
            rnp_try!(rnp_output_destroy(sink));
        },

        SOP::ExtractCert { no_armor, } => {
            let input = rnp_stdin()?;
            rnp_try!(rnp_load_keys(ffi, GPG, input,
                                   RNP_LOAD_SAVE_PUBLIC_KEYS));
            rnp_try!(rnp_input_destroy(input));

            let output = rnp_stdout()?;
            if no_armor {
                rnp_try!(rnp_save_keys(ffi, GPG, output,
                                       RNP_LOAD_SAVE_PUBLIC_KEYS));
            } else {
                let output = rnp_armor(output, "public key")?;
                rnp_try!(rnp_save_keys(ffi, GPG, output,
                                       RNP_LOAD_SAVE_PUBLIC_KEYS));
                rnp_try!(rnp_output_finish(output));
                rnp_try!(rnp_output_destroy(output));
            }
            rnp_try!(rnp_output_destroy(output));
        },

        SOP::Sign { no_armor, as_, keys, } => {
            match as_ {
                SignAs::Binary => (),
                _ => return Err(Error::UnsupportedOption.into()),
            }

            let keys = load_keys(ffi, &keys)?;

            let input = rnp_stdin()?;
            let output = rnp_stdout()?;
            let mut op = null_mut();
            rnp_try!(rnp_op_sign_detached_create(&mut op as *mut _,
                                                 ffi, input, output));
            rnp_try!(rnp_op_sign_set_armor(op, ! no_armor));
            for key in &keys {
                rnp_try!(rnp_op_sign_add_signature(op, *key, null_mut()));
            }

            rnp_try!(rnp_op_sign_execute(op));

            rnp_try!(rnp_op_sign_destroy(op));
            rnp_try!(rnp_input_destroy(input));
            rnp_try!(rnp_output_destroy(output));
            for key in keys {
                rnp_try!(rnp_key_handle_destroy(key));
            }
        },

        SOP::Verify { not_before, not_after, signatures, certs, } => {
            let certs = load_certs(ffi, &certs)?;
            // We don't need the handles.
            for cert in certs {
                rnp_try!(rnp_key_handle_destroy(cert));
            }

            let mut sig_file = load_file(signatures)?;
            let mut sig_data = Vec::new();
            sig_file.read_to_end(&mut sig_data)?;

            let signatures = rnp_input_bytes_ref(&sig_data)?;
            let input = rnp_stdin()?;

            let mut op = null_mut();
            rnp_try!(rnp_op_verify_detached_create(
                &mut op as *mut _, ffi, input, signatures));
            rnp_try!(rnp_op_verify_execute(op));

            let result = report_signatures(op, not_before, not_after,
                                           &mut stdout());
            rnp_try!(rnp_op_verify_destroy(op));
            result?;
        },

        SOP::Encrypt { no_armor, as_, with_password, sign_with, certs, } =>
        {
            match as_ {
                EncryptAs::Binary => (),
                _ => return Err(Error::UnsupportedOption.into()),
            }

            let certs = load_certs(ffi, &certs)?;
            let sign_with = load_keys(ffi, &sign_with)?;

            let input = rnp_stdin()?;
            let output = rnp_stdout()?;
            let mut op = null_mut();
            rnp_try!(rnp_op_encrypt_create(&mut op as *mut _,
                                           ffi, input, output));
            rnp_try!(rnp_op_encrypt_set_armor(op, ! no_armor));

            for cert in &certs {
                rnp_try!(rnp_op_encrypt_add_recipient(op, *cert));
            }

            for key in &sign_with {
                rnp_try!(rnp_op_encrypt_add_signature(op, *key, null_mut()));
            }

            for p in frob_passwords(with_password)?
                .into_iter().map(|p| CString::new(p))
            {
                rnp_try!(rnp_op_encrypt_add_password(op,
                                                     p?.as_ptr(),
                                                     null(), 0, null()));
            }

            rnp_try!(rnp_op_encrypt_execute(op));

            rnp_try!(rnp_op_encrypt_destroy(op));
            rnp_try!(rnp_input_destroy(input));
            rnp_try!(rnp_output_destroy(output));
            for cert in certs {
                rnp_try!(rnp_key_handle_destroy(cert));
            }
            for key in sign_with {
                rnp_try!(rnp_key_handle_destroy(key));
            }
        },

        SOP::Decrypt {
            session_key_out,
            with_session_key,
            with_password,
            verify_out,
            verify_with,
            verify_not_before,
            verify_not_after,
            key,
        } => {
            if session_key_out.is_some() {
                return Err(Error::UnsupportedOption.into());
            }

            if ! with_session_key.is_empty() {
                return Err(Error::UnsupportedOption.into());
            }

            unsafe extern "C"
            fn password_cb(_: rnp_ffi_t,
                           cookie: *mut c_void,
                           _: rnp_key_handle_t,
                           _: *const c_char,
                           buf: *mut c_char,
                           len: size_t)
                           -> bool {
                let passwords: &mut Vec<String> = std::mem::transmute(cookie);
                if let Some(p) = passwords.pop() {
                    // XXX: If p doesn't fit buf, tough luck.
                    let buf =
                        std::slice::from_raw_parts_mut(buf as *mut u8, len);
                    let l = buf.len().min(p.len());
                    let _ = &mut buf[..l].copy_from_slice(&p.as_bytes()[..l]);
                    true
                } else {
                    false
                }
            }

            let mut with_password = frob_passwords(with_password)?;
            if ! with_password.is_empty() {
                rnp_try!(rnp_ffi_set_pass_provider(
                    ffi, Some(password_cb),
                    &mut with_password as *mut _ as *mut c_void));
            }

            let certs = load_certs(ffi, &verify_with)?;
            // We don't need the handles.
            for cert in certs {
                rnp_try!(rnp_key_handle_destroy(cert));
            }

            let keys = load_keys(ffi, &key)?;
            // We don't need the handles.
            for cert in keys {
                rnp_try!(rnp_key_handle_destroy(cert));
            }

            let input = rnp_stdin()?;
            let output = rnp_stdout()?;

            let mut op = null_mut();
            rnp_try!(rnp_op_verify_create(
                &mut op as *mut _, ffi, input, output));
            rnp_try!(rnp_op_verify_execute(op));

            if let Some(verify_out) = verify_out {
                let mut sink = create_file(verify_out)?;

                // The result of the operation is not affected by the
                // signature verification.
                let _ = report_signatures(op,
                                          verify_not_before,
                                          verify_not_after,
                                          &mut sink);
            }
            rnp_try!(rnp_op_verify_destroy(op));
        },

        SOP::Unsupported(args) => {
            return Err(anyhow::Error::from(Error::UnsupportedSubcommand))
                .context(format!("Subcommand {} is not supported", args[0]));
        },

        _ => {
            return Err(anyhow::Error::from(Error::UnsupportedSubcommand))
                .context(format!("Subcommand is not supported"));
        },
    }

    rnp_try!(rnp_ffi_destroy(ffi));
    Ok(())
}

unsafe fn report_signatures(op: rnp_op_verify_t,
                            not_before: Option<DateTime<Utc>>,
                            not_after: Option<DateTime<Utc>>,
                            sink: &mut dyn Write) -> Result<()> {
    // Iterate over the signatures.
    let mut valid = 0;
    let mut count = 0;
    rnp_try!(rnp_op_verify_get_signature_count(
        op, &mut count as *mut _));
    for i in 0..count {
        let mut sig = null_mut();
        rnp_try!(rnp_op_verify_get_signature_at(
            op, i, &mut sig as *mut _));

        // Check the signature.
        let status = rnp_op_verify_signature_get_status(sig);
        if status != RNP_SUCCESS {
            eprintln!("Signature not valid: {}",
                      rnp_result_to_str(status));
            continue;
        }

        // Handle signature creation times.
        let mut creation_time = 0u32;
        rnp_try!(rnp_op_verify_signature_get_times(
            sig, &mut creation_time as *mut _, null_mut()));
        let creation_time = DateTime::<Utc>::from(
            UNIX_EPOCH + Duration::new(creation_time as u64, 0));

        if let Some(not_before) = not_before {
            if creation_time < not_before {
                eprintln!("Signature was created before \
                           the --not-before date.");
                continue;
            }
        }

        if let Some(not_after) = not_after {
            if creation_time > not_after {
                eprintln!("Signature was created after \
                           the --not-after date.");
                continue;
            }
        }

        // Get fingerprints.
        let mut handle = null_mut();
        rnp_try!(rnp_op_verify_signature_get_key(
            sig, &mut handle as *mut _));

        let mut key_raw = null_mut();
        rnp_try!(rnp_key_get_fprint(
            handle, &mut key_raw as *mut _));
        let key = CStr::from_ptr(key_raw)
            .to_str().expect("utf8").to_string();
        rnp_buffer_destroy(key_raw as *mut _);

        // Get the cert's fingerprint.  Note that we must not
        // call rnp_key_get_primary_fprint if handle refers to
        // the primary key fingerprint already.
        let mut is_primary = false;
        rnp_try!(rnp_key_is_primary(handle, &mut is_primary));
        let cert = if is_primary {
            key.clone()
        } else {
            let mut cert_raw = null_mut();
            rnp_try!(rnp_key_get_primary_fprint(
                handle, &mut cert_raw as *mut _));
            let cert = CStr::from_ptr(cert_raw)
                .to_str().expect("utf8").to_string();
            rnp_buffer_destroy(cert_raw as *mut _);
            cert
        };

        rnp_try!(rnp_key_handle_destroy(handle));

        // Finally, report.
        writeln!(sink, "{} {} {}", creation_time.format("%Y-%m-%dT%H:%M:%SZ"), key, cert)?;
        valid += 1;
    }

    if valid == 0 {
        Err(Error::NoSignature.into())
    } else {
        Ok(())
    }
}

unsafe fn rnp_stdout() -> Result<rnp_output_t> {
    let mut output = null_mut();
    unsafe extern "C" fn writer(_cookie: *mut c_void,
                               buf: *const c_void,
                               len: size_t)
                               -> bool
    {
        let buf = std::slice::from_raw_parts(buf as *const u8, len);
        stdout().write_all(buf).is_ok()
    }
    unsafe extern "C" fn closer(_cookie: *mut c_void,
                               _discard: bool)
    {
        // Do nothing.
    }
    rnp_try!(rnp_output_to_callback(&mut output as *mut _,
                                    Some(writer),
                                    Some(closer),
                                    null_mut()));
    Ok(output)
}

unsafe fn rnp_stdin() -> Result<rnp_input_t> {
    let mut input = null_mut();
    unsafe extern "C" fn reader(_cookie: *mut c_void,
                                buf: *mut c_void,
                                len: size_t,
                                read: *mut size_t)
                               -> bool
    {
        let buf = std::slice::from_raw_parts_mut(buf as *mut u8, len);
        match stdin().read(buf) {
            Ok(n) => {
                *read = n;
                true
            },
            Err(_) => false,
        }
    }
    unsafe extern "C" fn closer(_cookie: *mut c_void)
    {
        // Do nothing.
    }
    rnp_try!(rnp_input_from_callback(&mut input as *mut _,
                                     Some(reader),
                                     Some(closer),
                                     null_mut()));
    Ok(input)
}

/// Constructs an input from a buffer.
///
/// ATTENTION: Implicit lifetimes!  bytes must outlive the returned
/// rnp_input_t.
unsafe fn rnp_input_bytes_ref(bytes: &[u8]) -> Result<rnp_input_t> {
    let mut input = null_mut();
    rnp_try!(rnp_input_from_memory(&mut input as *mut _,
                                   bytes.as_ptr(),
                                   bytes.len(),
                                   false)); // do not copy
    Ok(input)
}

unsafe fn rnp_armor(sink: rnp_output_t, typ: &str) -> Result<rnp_output_t> {
    let mut output = null_mut();
    rnp_try!(rnp_output_to_armor(
        sink,
        &mut output as *mut _,
        CString::new(typ.as_bytes().to_vec())?.as_ptr()));
    Ok(output)
}
